# Vocab App Front-End

## Description ##
A serverless webapp that interacts with a Notion database, and leverages that data to generate wallpaper images.
* Populates images with data received from an open-source dictionary API
* Built using Lambda, API Gateway, SNS, S3
* Uses CI/CD pipelines to test, plan and deploy
* Infrastructure is built using Terraform

## Architecture ##
<br />
![vocab-app-diagram](https://gitlab.com/vocab-app/vocab-app-back-end/-/raw/main/img/vocab-app-design.png)
